# Neos CMS - Swiper Plugin
A small node type based swiper plugin for Neos CMS.

## Installation
Add the package in your site package composer.json
```
"require": {
    "obisconcept/neos-swiper-plugin": "~1.1"
}
```

## Version History

### Changes in 1.1.1
- Minor bugfix

### Changes in 1.1.0
- Added bundled assets. Complete rebuilding 

### Changes in 1.0.1
- Critical Bugfix - Conflict with Bootstrap Package

### Changes in 1.0.0
- First version of the Neos CMS swiper plugin
- Swiper JavaScript library in version 3.1.0
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var UglifyJsPlugin = require('webpack').optimize.UglifyJsPlugin;

var ROOT_PATH = path.resolve(__dirname);
var SRC_PATH = path.resolve(ROOT_PATH, 'Resources/Private/Assets');
var DIST_PATH = path.resolve(ROOT_PATH, 'Resources/Public');

module.exports = {
    entry: {
        normal: SRC_PATH + '/entry-normal.js',
        'backend': SRC_PATH + '/entry-backend.js',
    },
    output: {
        path: DIST_PATH,
        filename: 'js/bundle-[name].js'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css?sourceMap&minimize!sass?sourceMap'),
                include: SRC_PATH
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/bundle-[name].css'),
        new UglifyJsPlugin({
            sourceMap: true
        })
    ]
};